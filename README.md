# GraphQL Crash Course
This project implements the project tracking system described in <https://github.com/bradtraversy/project-mgmt-graphql>. Some modifications have been made to improve performance -
1. All queries use cache updates instead of query refetching to prevent additional requests to the server
